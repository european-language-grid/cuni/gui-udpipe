FROM nginx:alpine

COPY index.html /usr/share/nginx/html
COPY js-treex-view.min.js /usr/share/nginx/html

COPY standalone_demo.html /usr/share/nginx/html
